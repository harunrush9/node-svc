# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.37.0"
  constraints = "~> 3.37.0"
  hashes = [
    "h1:3i4E08/CvAq4aEeQuzhGTJK5U5T+2PdzuNRPjnpgIno=",
    "zh:0b94fc8586a417422b5b50a3011dd093973048238e3d1934857cb4aa1c8550c4",
    "zh:0ced7f36f4dc2e440f5c188e872347ac94bf3811226aeeea50e31c0e55971d34",
    "zh:1a6556202e66f216ae6bd4fbdb279591a898aeab89ed4ea55706b88a25161f7b",
    "zh:1e40c2db119ef360ad6fe3216da8810f37d552ac1d57c869165d3c0924b1ce07",
    "zh:37826f6ea11a4be6b9bb33ffc4da7a038a91fc02a352ecb4b29397e44be85c8a",
    "zh:396c79600a47a706a06fc5d016ed1e4fb4cede7b0b28a9887fea27a598789e53",
    "zh:5c751fb41cb4b168b5886f607998ed0ccfee924fd1425449c221162871b08ba3",
    "zh:6e5095eaffeb73a91203338815f7d9f9dd507b8e52bb88cc38012cec3f2c2809",
    "zh:91b9da1a2ac0daa30b30f2adf0118b0882fbd64a55bccf88d53ebcc5dcfcc0fd",
    "zh:b622345284e12f822cd113bdaa0db51b4e47345e15c85f15c3a302406353d654",
  ]
}
